import random

import nn
import algebra

def main():
    net = nn.Network([2, 2, 1], nn.Atan())
    for _ in range(100000):
        x = random.randint(0, 1)
        y = random.randint(0, 1)
        inp = algebra.Matrix.vector([float(x), float(y)])
        result = net.feedforward(inp)
        desired = algebra.Matrix.vector([float(x ^ y)])
        cost = nn.cost(result, desired)
        net.update(inp, desired, cost)

    for x in range(2):
        for y in range(2):
            inp = algebra.Matrix.vector([float(x), float(y)])
            result = net.feedforward(inp)
            print('{} ^ {} = {}'.format(x, y, result.get(0, 0)))

if __name__ == '__main__':
    main()
