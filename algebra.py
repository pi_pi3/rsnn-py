class Matrix:
    def __init__(self, rows, cols, storage):
        self.rows = rows
        self.cols = cols
        self.storage = storage

    def zero(rows, cols):
        return Matrix(rows, cols, [0.0 for x in range(rows * cols)])

    def one(rows_cols):
        output = Matrix.zero(rows_cols, rows_cols)
        for i in range(rows_cols):
            output.set(i, i, 1.0)
        return output

    def matrix(rows, cols, storage):
        return Matrix(rows, cols, storage)

    def vector(storage):
        return Matrix(len(storage), 1, storage)

    def inner(self):
        return self.storage

    def get(self, i, j):
        assert j < self.cols, "index out of bounds"
        assert i < self.rows, "index out of bounds"
        return self.storage[i * self.cols + j]

    def set(self, i, j, value):
        assert j < self.cols, "index out of bounds"
        assert i < self.rows, "index out of bounds"
        self.storage[i * self.cols + j] = value

    def component_mul(self, other):
        assert self.rows == other.rows, "incompatible matrices"
        assert self.cols == other.cols, "incompatible matrices"
        output = Matrix.zero(self.rows, self.cols)
        for j in range(self.cols):
            for i in range(self.rows):
                x = self.get(i, j) * other.get(i, j)
                output.set(i, j, x)
        return output

    def transpose(self):
        output = Matrix.zero(self.cols, self.rows)
        for j in range(self.cols):
            for i in range(self.rows):
                x = self.get(i, j)
                output.set(j, i, x)
        return output

    def map(self, f):
        return Matrix(self.rows, self.cols, [f(x) for x in self.storage])

    def __neg__(self):
        output = Matrix.zero(self.rows, self.cols)
        for j in range(self.cols):
            for i in range(self.rows):
                x = -self.get(i, j)
                output.set(i, j, x)
        return output

    def __add__(self, other):
        assert self.rows == other.rows, "incompatible matrices"
        assert self.cols == other.cols, "incompatible matrices"
        output = Matrix.zero(self.rows, self.cols)
        for j in range(self.cols):
            for i in range(self.rows):
                x = self.get(i, j) + other.get(i, j)
                output.set(i, j, x)
        return output

    def __sub__(self, other):
        assert self.rows == other.rows, "incompatible matrices"
        assert self.cols == other.cols, "incompatible matrices"
        output = Matrix.zero(self.rows, self.cols)
        for j in range(self.cols):
            for i in range(self.rows):
                x = self.get(i, j) - other.get(i, j)
                output.set(i, j, x)
        return output

    def __mul__(self, other):
        if isinstance(other, Matrix):
            assert self.cols == other.rows, "incompatible matrices"
            output = Matrix.zero(self.rows, other.cols)
            for j in range(other.cols):
                for i in range(self.rows):
                    acc = 0.0
                    for k in range(self.cols):
                        acc += self.get(i, k) * other.get(k, j)
                    output.set(i, j, acc)
            return output
        elif isinstance(other, float) or isinstance(other, int):
            output = Matrix.zero(self.rows, self.cols)
            for j in range(self.cols):
                for i in range(self.rows):
                    x = self.get(i, j) * other
                    output.set(i, j, x)
            return output
        else:
            raise TypeError('expected Matrix, float or int, got {}'.format(type(other)))

    def __str__(self):
        output = '' 
        for i in range(self.rows):
            output += '| '
            for j in range(self.cols):
                output += '{} '.format(self.get(i, j))
            output += '|\n'
        return output

    def __repr__(self):
        output = '[' 
        for i in range(self.rows):
            for j in range(self.cols):
                output += ' {}'.format(self.get(i, j))
            output += ';'
        output += ' ]' 
        return output
