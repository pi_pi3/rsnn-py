import math
import random
import numpy

from algebra import Matrix

def cost(result, desired):
    diff = result - desired
    return sum(diff.component_mul(diff).inner())

class Activation:
    def call(self, n):
        return None

    def derivative(self, n):
        return None


class Relu(Activation):
    def __init__(self):
        pass

    def call(self, n):
        return max(n, 0.0)

    def derivative(self, n):
        return 0.0 if n < 0.0 else 1.0


class Tanh(Activation):
    def __init__(self):
        pass

    def call(self, n):
        return numpy.tanh(n)

    def derivative(self, n):
        t = numpy.tanh(n)
        return 1.0 - t * t


class Atan(Activation):
    def __init__(self):
        pass

    def call(self, n):
        return math.atan(n)

    def derivative(self, n):
        return 1.0 / (n * n + 1.0)


class Sigmoid(Activation):
    def __init__(self):
        pass

    def call(self, n):
        return 1.0 / (1.0 + math.exp(-n))

    def derivative(self, n):
        sigmoid = self.call(n)
        return sigmoid * (1.0 - sigmoid)


class Network:
    def __init__(self, layers, act):
        weights = []
        biases = []
        for inp, outp in zip(layers, layers[1:]):
            weight_size = inp * outp
            weight = [random.gauss(0.0, 1.0) for _ in range(weight_size)]
            weights.append(Matrix(outp, inp, weight))
            bias = [random.gauss(0.0, 1.0) for _ in range(outp)]
            biases.append(Matrix.vector(bias))
        self.weights = weights
        self.biases = biases
        self.act = act

    def layers(self):
        return len(self.weights)

    def feedforward(self, layer):
        for weight, bias in zip(self.weights, self.biases):
            layer = (weight * layer + bias).map(lambda n: self.act.call(n))
        return layer

    def update(self, inp, desired, eta):
        nabla_b, nabla_w = self.backprop(inp, desired)
        
        self.weights = [w - nabla * eta for w, nabla in zip(self.weights, nabla_w)]
        self.biases = [b - nabla * eta for b, nabla in zip(self.biases, nabla_b)]

    def backprop(self, inp, desired):
        def nabla_w_l(activation, delta):
            output = Matrix.zero(delta.rows, activation.rows)
            for i in range(delta.rows):
                for j in range(activation.rows):
                    x = activation.get(j, 0) * delta.get(i, 0)
                    output.set(i, j, x)
            return output

        nabla_b = [Matrix.zero(b.rows, b.cols) for b in self.biases]
        nabla_w = [Matrix.zero(w.rows, w.cols) for w in self.weights]
        activations = [inp]
        activation = activations[-1]
        zs = []

        for bias, weight in zip(self.biases, self.weights):
            z = weight * activation + bias
            activations.append(z.map(lambda z: self.act.call(z)))
            activation = activations[-1]
            zs.append(z)

        delta = (activation - desired).component_mul(zs[-1].map(lambda z: self.act.derivative(z)))
        nabla_w[-1] = nabla_w_l(activations[-2], delta)
        nabla_b[-1] = delta
        for l in range(2, self.layers() + 1):
            z = zs[-l]
            der = z.map(lambda z: self.act.derivative(z))
            delta = (self.weights[-l + 1].transpose() * nabla_b[-l + 1]).component_mul(der)
            nabla_w[-l] = nabla_w_l(activations[-l - 1], delta)
            nabla_b[-l] = delta

        return nabla_b, nabla_w
